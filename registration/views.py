import datetime

from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import render
from django.http import HttpResponse

from registration.models import UserJJUniversum


def registration(request):
    if request.method == 'GET':
        context = {}
        return render(request, 'registration.html', context)

    elif request.method == 'POST' and 'register' in request.POST:
        first_name = request.POST.get("first_name_registration")
        last_name = request.POST.get("last_name_registration")
        username = request.POST.get("username_registration")
        email = request.POST.get("email_registration")
        password = request.POST.get("password_registration")
        dob = request.POST.get("dob_registration")

        if User.objects.filter(username=username).exists():
            context = {'username_exist': username}
            messages.add_message(request, messages.INFO, 'Hello world.')
            return render(request, 'registration.html', context)
        else:
            dob = datetime.datetime.strptime(dob, "%m/%d/%Y").strftime("%Y-%m-%d")

            user = User.objects.create_user(username, email, password)
            user.first_name = first_name
            user.last_name = last_name
            user.save()

            UserJJUniversum.objects.create(
                user=user,
                dob=dob
            )

        return HttpResponse("Creado")
