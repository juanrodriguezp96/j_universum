from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
import django
import uuid


class UserJJUniversum(models.Model):
    create_date = models.DateField(default=django.utils.timezone.now)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    dob = models.DateField(max_length=8)
